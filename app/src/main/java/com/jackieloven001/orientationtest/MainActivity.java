package com.jackieloven001.orientationtest;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends Activity implements SensorEventListener {

    SoundPool sp;
    int leftSideDown = 0;
    int rightSideDown = 0;
    // start at 0 so an error doesn't come up if it isn't loaded yet
    int soundPlaying = 0;
    // if it is 0, no sound is playing, if it is 1, sound is playing

    // Accelerometer X, Y, and Z values
    private TextView accelXValue;
    private TextView accelYValue;
    private TextView accelZValue;

    private TextView xTextBox;
    private TextView yTextBox;

    private SensorManager sensorManager = null;

    /**
     * Called when the activity is first created.
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        // Get a reference to a SensorManager
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        setContentView(R.layout.activity_main);

        sp = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        // The "1" is the max number of sounds that can be played at once
        leftSideDown = sp.load(this, R.raw.left_side_down, 1);
        rightSideDown = sp.load(this, R.raw.right_side_down, 1);
        // if it messes up try changing the priority since right now
        // they are both the same (1)

        // Capture accelerometer related view elements
        accelXValue = (TextView) findViewById(R.id.accel_x_value);
        accelYValue = (TextView) findViewById(R.id.accel_y_value);
        accelZValue = (TextView) findViewById(R.id.accel_z_value);

        xTextBox = (TextView) findViewById(R.id.textView01);
        yTextBox = (TextView) findViewById(R.id.textView02);

        // Initialize accelerometer related view elements
        accelXValue.setText("0.00");
        accelYValue.setText("0.00");
        accelZValue.setText("0.00");

    }

    // This method will update the UI on new sensor events
    public void onSensorChanged(SensorEvent sensorEvent) {

        synchronized (this) {
            if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                accelXValue.setText(Float.toString(sensorEvent.values[0]));
                accelYValue.setText(Float.toString(sensorEvent.values[1]));
                accelZValue.setText(Float.toString(sensorEvent.values[2]));


                if (sensorEvent.values[0] > 0.3) {
                    xTextBox.setText("Tilt the right side down");
                    if (rightSideDown != 0) {
                        sp.play(rightSideDown, 1, 1, 1, 0, 1);
                    }

                } else if (sensorEvent.values[0] < -0.3) {
                    xTextBox.setText("Tilt the left side down");
                    if (leftSideDown != 0) {
                        sp.play(leftSideDown, 1, 1, 0, 0, 1);
                        // soundID, leftVolume (1 is highest), rightVolume, priority, loop (want this to be 0)
                        // rate (how fast, must be fraction/multiple of original
                    }
                } else {
                    xTextBox.setText("Pretty flat!");
                }

                if (sensorEvent.values[1] > 0.3) {
                    yTextBox.setText("Tilt the top down");
                } else if (sensorEvent.values[1] < -0.3) {
                    yTextBox.setText("Tilt the bottom down");
                } else {
                    yTextBox.setText("Pretty flat!");
                }


            }


        }
    }

    // I've chosen to not implement this method
    public void onAccuracyChanged(Sensor arg0, int arg1) {
// TODO Auto-generated method stub
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Register this class as a listener for the accelerometer sensor
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onStop() {
        // Unregister the listener
        sensorManager.unregisterListener(this);
        super.onStop();
    }
}